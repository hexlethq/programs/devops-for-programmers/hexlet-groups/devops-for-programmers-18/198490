terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "irinwork42" {
  name = "irinwork42"
}

# Create a web server
resource "digitalocean_droplet" "web" {
    image = "docker-20-04"
    name = "web-terraform-homework-01"
    region = "ams3"
    size = "s-1vcpu-1gb"
    ssh_keys = [data.digitalocean_ssh_key.irinwork42.id]
}

resource "digitalocean_droplet" "web2" {
    image = "docker-20-04"
    name = "web-terraform-homework-02"
    region = "ams3"
    size = "s-1vcpu-1gb"
    ssh_keys = [data.digitalocean_ssh_key.irinwork42.id]
}

resource "digitalocean_loadbalancer" "lb" {

    name = "lb"
    region = "ams3"
  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = [digitalocean_droplet.web.id, digitalocean_droplet.web2.id]
}

resource "digitalocean_domain" "default" {
  name       = "terra.irkin.club"
  ip_address = digitalocean_loadbalancer.lb.ip
}

output "output_name" {
  value = [
    digitalocean_droplet.web.ipv4_address,
    digitalocean_droplet.web2.ipv4_address
  ]
}


