terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "ssh_key" {
  name = var.do_login
}

resource "digitalocean_vpc" "vpc" {
  name   = "${var.homework}-vpc"
  region = "ams3"
}

resource "digitalocean_firewall" "web-firewall" {
  name = "web-firewall"

  droplet_ids = digitalocean_droplet.web.*.id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "53"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "5000"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_droplet_ids = [digitalocean_droplet.bastion.id]
  }

  inbound_rule {
    protocol              = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol              = "tcp"
    port_range = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
  
  inbound_rule {
    protocol              = "tcp"
    port_range = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol         = "tcp"
    port_range       = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range = "80"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
  
  outbound_rule {
    protocol              = "tcp"
    port_range = "443"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_firewall" "bastion-firewall" {
  name = "bastion-firewall"

  droplet_ids = [digitalocean_droplet.bastion.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol              = "icmp"
    source_droplet_ids = digitalocean_droplet.web.*.id
  }

  outbound_rule {
    protocol              = "tcp"
    port_range       = "22"
    destination_droplet_ids = digitalocean_droplet.web.*.id
  }

  outbound_rule {
    protocol              = "icmp"
    destination_droplet_ids = digitalocean_droplet.web.*.id
  }
}

resource "digitalocean_droplet" "web" {
    count = 2
    image = "ubuntu-18-04-x64"
    name = "${var.homework}-${count.index}"
    region = "ams3"
    size = "s-1vcpu-1gb"
    vpc_uuid = digitalocean_vpc.vpc.id
    ssh_keys = [data.digitalocean_ssh_key.ssh_key.id]
}

resource "digitalocean_droplet" "bastion" {
    image = "ubuntu-18-04-x64"
    name = "${var.homework}-bastion"
    region = "ams3"
    size = "s-1vcpu-1gb"
    vpc_uuid = digitalocean_vpc.vpc.id
    ssh_keys = [data.digitalocean_ssh_key.ssh_key.id]
}

resource "digitalocean_domain" "domain" {
  name       = "${var.homework}.irkin.club"
}

resource "digitalocean_certificate" "cert" {
  name    = "${var.homework}.cert"
  type    = "lets_encrypt"
  domains = [digitalocean_domain.domain.name]
}

resource "digitalocean_loadbalancer" "lb" {

  name = "${var.homework}-lb"
  region = "ams3"
  redirect_http_to_https = true
  vpc_uuid = digitalocean_vpc.vpc.id
  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 5000
    target_protocol = "http"
    certificate_name = digitalocean_certificate.cert.name
  }

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "tcp"
  }

  droplet_ids = digitalocean_droplet.web.*.id
}

output "bastion" {
  value       = digitalocean_droplet.bastion.ipv4_address
}

output "droplets" {
  value = digitalocean_droplet.web.*.ipv4_address_private
}
