terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "irinwork42" {
  name = "irinwork42"
}

# Create a web server
resource "digitalocean_droplet" "web" {
    count = 2
    image = "docker-20-04"
    name = "${var.homework}-${count.index}"
    region = "ams3"
    size = "s-1vcpu-1gb"
    ssh_keys = [data.digitalocean_ssh_key.irinwork42.id]
}

resource "digitalocean_loadbalancer" "lb" {

  name = "${var.homework}-lb"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 22
    protocol = "tcp"
  }

  droplet_ids = digitalocean_droplet.web.*.id
}

resource "digitalocean_domain" "default" {
  name       = "${var.homework}.irkin.club"
  ip_address = digitalocean_loadbalancer.lb.ip
}

output "droplets" {
  value = [for item in digitalocean_droplet.web : {
    host = item.ipv4_address
    name = item.name
  }]
}

output "ssh_key" {
  value = data.digitalocean_ssh_key.irinwork42
}
