module.exports = ({ env }) => ({
  url: env("SERVER_URL"),
    admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET'),
    },
  },
});
